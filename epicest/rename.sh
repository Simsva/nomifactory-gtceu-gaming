#!/bin/sh

die() {
    echo "$1" 1>&2
    exit 1
}

machine_name_volt() {
    printf "gregtech\.machine\.[a-z_]+\.%s\.name" "$1"
}

usage="usage: $0 langfile"

name_char="[a-zA-Z \-_]"

rules="\
$(machine_name_volt "[a-z_]+")|Advanced ($name_char+) II|Advanceder \2
$(machine_name_volt "[a-z_]+")|Advanced ($name_char+) III|Advancedest \2
$(machine_name_volt "[a-z_]+")|Elite ($name_char+) II|Eliter \2
$(machine_name_volt "[a-z_]+")|Elite ($name_char+) III|Elitest \2
$(machine_name_volt "[a-z_]+")|Epic ($name_char+) II|Epicer \2
$(machine_name_volt "[a-z_]+")|Epic ($name_char+) III|Epicest \2
$(machine_name_volt "[a-z_]+")|Epic ($name_char+) IV|Epicestest \2"

rules_c="$(echo "$rules" | wc -l)"

infile="$1"
[ -z "$infile" ] && die "$usage"
[ ! -f "$infile" ] && die "No such language file"

lines=""
for i in $(seq "$rules_c"); do
    rule="$(printf    "%s" "$rules" | cut -d'
' -f"$i")"
    id="$(printf      "%s" "$rule" | cut -d'|' -f1)"
    oldname="$(printf "%s" "$rule" | cut -d'|' -f2)"
    newname="$(printf "%s" "$rule" | cut -d'|' -f3)"
    newlines="$(grep -E "$id=$oldname" "$infile")"
    if [ "$newlines" ]; then
      lines="$newlines
$lines"
      cmd="s/^($id)=$oldname$/\1=$newname/;$cmd"
    fi
done

lines="${lines%%
}"
echo "$lines" | sed -E "$cmd"
