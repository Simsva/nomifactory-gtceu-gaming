## script for simulating different oil-cracking setups and generating statistics.
## can be imported as a module or executed to produce Markdown tables

# ratios for different fuels (butadiene includes butane and butene)
# (light fuel, heavy fuel, naphtha, butadiene, ethylene, ethane)
## light fuel
lightly_hydro_light  = (0, 0,   .8, .15,  0,   .125)
severely_hydro_light = (0, 0,   .2, .125, 0,   1.5)
lightly_steam_light  = (0, .15, .4, .135, .05, .01)
severely_steam_light = (0, .05, .1, .115, .25, .05)

## heavy fuel
lightly_hydro_heavy  = (.6, 0, .1,   .1,  0,   .075)
severely_hydro_heavy = (.2, 0, .25,  .3,  0,   .175)
lightly_steam_heavy  = (.3, 0, .05,  .04, .05, .005)
severely_steam_heavy = (.1, 0, .125, .13, .15, .015)

## naphtha
lightly_hydro_naphtha  = (0,   0,    0, .8,   0,  .25)
severely_hydro_naphtha = (0,   0,    0, .125, 0,  1.5)
lightly_steam_naphtha  = (.15, .075, 0, .23,  .2, .035)
severely_steam_naphtha = (.05, .025, 0, .1,   .5, .065)

# fuels per oil recipe (light fuel, heavy fuel, naphtha, sulfuric gas)
fuel_oil       = (50,  15, 20)
fuel_raw_oil   = (50,  15, 20)
fuel_light_oil = (20,  10, 30)
fuel_heavy_oil = (45, 250, 15)

# minimum amount of fuel per recipe
fuel_oil_min = 50
fuel_raw_oil_min = 100
fuel_light_oil_min = 150
fuel_heavy_oil_min = 100

def fuel_calc(l, h, n, chosen_light, chosen_heavy, chosen_naphtha, iters = 1):
    lpl, hpl, npl, bpl, eypl, epl = chosen_light
    lph, hph, nph, bph, eyph, eph = chosen_heavy
    lpn, hpn, npn, bpn, eypn, epn = chosen_naphtha
    # butadiene, ethylene, ethane
    but, ethy, eth = 0, 0, 0
    for i in range(iters):
        nl    = lpl*l  + lph*h  + lpn*n  + 0*but + 0*ethy + 0*eth
        nh    = hpl*l  + hph*h  + hpn*n  + 0*but + 0*ethy + 0*eth
        nn    = nph*h  + npl*l  + npn*n  + 0*but + 0*ethy + 0*eth
        nbut  = bpl*l  + bph*h  + bpn*n  + 1*but + 0*ethy + 0*eth
        nethy = eypl*l + eyph*h + eypn*n + 0*but + 1*ethy + 0*eth
        neth  = epl*l  + eph*h  + epn*n  + 0*but + 0*ethy + 1*eth
        l, h, n, but, ethy, eth = nl, nh, nn, nbut, nethy, neth
    return l, h, n, but, ethy, eth

light_types = [
    ("Lightly Hydro-Cracked",  lightly_hydro_light),
    ("Severely Hydro-Cracked", severely_hydro_light),
    ("Lightly Steam-Cracked",  lightly_steam_light),
    ("Severely Steam-Cracked", severely_steam_light),
]
heavy_types = [
    ("Lightly Hydro-Cracked",  lightly_hydro_heavy),
    ("Severely Hydro-Cracked", severely_hydro_heavy),
    ("Lightly Steam-Cracked",  lightly_steam_heavy),
    ("Severely Steam-Cracked", severely_steam_heavy),
]
naphtha_types = [
    ("Lightly Hydro-Cracked",  lightly_hydro_naphtha),
    ("Severely Hydro-Cracked", severely_hydro_naphtha),
    ("Lightly Steam-Cracked",  lightly_steam_naphtha),
    ("Severely Steam-Cracked", severely_steam_naphtha),
]
oil_types = [
    ("Oil",       tuple(x/fuel_oil_min       for x in fuel_oil)),
    ("Raw Oil",   tuple(x/fuel_raw_oil_min   for x in fuel_raw_oil)),
    ("Light Oil", tuple(x/fuel_light_oil_min for x in fuel_light_oil)),
    ("Heavy Oil", tuple(x/fuel_heavy_oil_min for x in fuel_heavy_oil)),
]

if __name__ == "__main__":
    import itertools as it
    import json

    # simulations
    best = {
        "Butadiene": {
            "Olja": dict(),
            "Fuel": dict(),
        },
        "Ethylene": {
            "Olja": dict(),
            "Fuel": dict(),
        },
        "Ethane": {
            "Olja": dict(),
            "Fuel": dict(),
        },
    }
    iters = 10
    for on, ot in oil_types:
        best_but = -1
        best_but_res = None
        best_but_fuel = None
        best_ethy = -1
        best_ethy_res = None
        best_ethy_fuel = None
        best_eth = -1
        best_eth_res = None
        best_eth_fuel = None

        for (ln, lt), (hn, ht), (nn, nt) in it.product(light_types, heavy_types, naphtha_types):
            l, h, n, but, ethy, eth = fuel_calc(*ot, lt, ht, nt, iters)
            fuel = {
                "Light Fuel": ln,
                "Heavy Fuel": hn,
                "Naphtha":    nn,
            }
            res = {
                "Butadiene/1000 olja": round(1000*but),
                "Ethylene/1000 olja":  round(1000*ethy),
                "Ethane/1000 olja":    round(1000*eth),
            }
            if but > best_but:
                best_but = but
                best_but_res = res
                best_but_fuel = fuel
            if ethy > best_ethy:
                best_ethy = ethy
                best_ethy_res = res
                best_ethy_fuel = fuel
            if eth > best_eth:
                best_eth = eth
                best_eth_res = res
                best_eth_fuel = fuel

        # best fuel types are the same for each oil type
        best["Butadiene"]["Fuel"] = best_but_fuel
        best["Ethylene"]["Fuel"] = best_ethy_fuel
        best["Ethane"]["Fuel"] = best_eth_fuel

        best["Butadiene"]["Olja"][on] = best_but_res
        best["Ethylene"]["Olja"][on] = best_ethy_res
        best["Ethane"]["Olja"][on] = best_eth_res

    def format_table(header, data):
        out = f"# {header}\n\n"

        out += "| Fuel | Bäst typ |\n| ---- | -------- |\n"
        for fuel, typ in data["Fuel"].items():
            out += f"| {fuel} | {typ} |\n"
        
        out += "\n| Oljetyp | Ämne | Mängd |\n| ------- | ---- | -----:|\n"
        for category, values in data["Olja"].items():
            for i, (key, value) in enumerate(values.items()):
                out += "| {cat} | {key} | {val} |\n".format(
                    cat = category if i == 0 else "",
                    key = key,
                    val = value
                )
        return out

    #print(json.dumps(out))
    for resource, data in best.items():
        print(format_table(resource, data))
